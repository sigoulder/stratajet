class NotamsController < ApplicationController
  def index
  end

  def output
  	@notams = Notam.return_correct_notams params[:input]
  end
end
