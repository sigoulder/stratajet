class Notam < ActiveRecord::Base

	def self.days_array
		days = []
		Date::DAYNAMES.each_with_index { |x, i| days << x[0..2].upcase }
		return days
	end

	def self.get_extra_time_if_exists arr
		value =  arr[0]
		value = value + " #{arr[1]}"  if !arr[1].blank? && arr[1][0..2].to_i != 0
		return value
	end

	def self.return_correct_notams input
		notam_arr = Array.new
		input.split("\n\r").each do |n|
			notam_arr << n.gsub(/[^0-9a-z )-]/i, ' ').split("A) ")[1] if n.include? 'AERODROME HOURS OF OPS/SERVICE'
		end
		
		notam_arr.collect! {|x| x = x.split(' CREATED')[0] }

		details_array = Array.new
		notam_arr.each do |n|
			times_hash = Hash.new { |hash, key| hash["ICAO"] = n[0..4] }
			days_array.each do |d|
				if n.include?(d)
					from_index = n.index(d)
					if n[from_index + 3] == '-'
						next_space = n[from_index+7..n.size].split(' ')
						value = get_extra_time_if_exists next_space
						days_array[days_array.index(d)..days_array.index(n[from_index+4..from_index + 6])].each do |i|
							times_hash[i] = value
						end
					else
						next_space = n[from_index+3..n.size].split(' ')
						value = get_extra_time_if_exists next_space
					end
					times_hash[d] = value
				end
			end
			details_array << times_hash
		end
		return details_array
	end

end
